/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.enta.medeiros.traduzap;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jose_
 */
public class server {
    
    private SRV srv;
    private boolean run = true;

    void startSrv() {
        try {
            ServerSocket serverSocket = new ServerSocket(9009, 0, InetAddress.getByName("localhost"));
            escrever("Server on at: " + serverSocket.toString());
            escrever("Server waiting client's connection....");
            while (run) {
                Socket socket = serverSocket.accept();
                escrever("Client connected on: " + socket.toString());
                if (!run) {
                    break;
                }
               srv = new SRV(socket);
                srv.start();
            }
        } catch (Exception e) {
            Logger.getLogger(server.class.getName()).log(Level.SEVERE, null, e);
            escrever("Problema a terminar o servidor");
        }
    }

    class SRV extends Thread {
        Socket socket;
        ObjectOutputStream output;
        ObjectInputStream input;
        protected String[] frases = new String[]{
         "Welcome to my application",
         "Programming is fun"
        };
        SRV(Socket socket) throws ClassNotFoundException, SQLException {
            this.socket = socket;
            try {
                input = new ObjectInputStream(socket.getInputStream());
                output = new ObjectOutputStream(socket.getOutputStream());
            } catch (IOException ex) {
                Logger.getLogger(server.class.getName()).log(Level.SEVERE, null, ex);
                input = null;
                output = null;
            }
        }

        public void run() {
            boolean acabou = true;
            try {
                while (acabou) {
                    if (!acabou) {
                        break;
                    }
                    Protocolo protocolo = (Protocolo) input.readObject();
                    switch (protocolo.getType()) {
                        case Protocolo.EN:
                            //Traduz para ingles
                            int frase = protocolo.getFrase();
                            sendMsg(frases[frase]);
                            break;
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(SRV.class.getName()).log(Level.SEVERE, null, ex);
                escrever("Problema ao receber dados do cliente!");
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(server.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                socket.close();
            } catch (IOException ex) {
                Logger.getLogger(server.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        protected synchronized boolean sendMsg(String msg) {
            try {
                output.writeObject(msg);
                return true;
            } catch (IOException ex) {
                Logger.getLogger(SRV.class.getName()).log(Level.SEVERE, null, ex);
                escrever("Problema ao escrever para o cliente!");
                return false;
            }
        }
    }
    void escrever(String msg) {
        System.out.println(msg);
    }
}
