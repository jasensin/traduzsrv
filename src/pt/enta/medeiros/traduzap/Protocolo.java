package pt.enta.medeiros.traduzap;

import java.io.Serializable;

public class Protocolo implements Serializable {

    private static final long serialVersionUID = 1112122200L;
    public static final int EN = 0;
    public static final int FR = 1;
    public static final int AL = 2;
    public static final int RS = 3;

    private int type;
    private int Frase;
    private String msg;

    public Protocolo(int type, int Frase) {
        this.type = type;
        this.Frase = Frase;
    }

    public Protocolo(int type, String msg) {
        this.type = type;
        this.msg = msg;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getFrase() {
        return Frase;
    }

    public void setFrase(int Frase) {
        this.Frase = Frase;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
